// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LD_ls12GameMode.generated.h"

UCLASS(minimalapi)
class ALD_ls12GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALD_ls12GameMode();
};



