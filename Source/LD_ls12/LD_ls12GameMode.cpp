// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD_ls12GameMode.h"
#include "LD_ls12Character.h"
#include "UObject/ConstructorHelpers.h"

ALD_ls12GameMode::ALD_ls12GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
